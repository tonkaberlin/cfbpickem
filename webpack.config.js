const NODE_ENV = process.env.NODE_ENV;

const webpack = require("webpack");
const fs = require("fs");
const path = require("path"),
  join = path.join,
  resolve = path.resolve;

const getConfig = require("hjs-webpack");

const isDev = NODE_ENV === "development" || NODE_ENV === "test";
const isTest = NODE_ENV === "test";

const root = resolve(__dirname);
const src = join(root, "src")
const modules = join(root, "node_modules");
const dest = join(root, "dist");
const css = join(src, "styles")

var config = getConfig({
  isDev: isDev,
  in: join(src, "react/app.tsx"),
  out: "dist/public",
  html: function (context) {
    return {
      "index.html": context.defaultTemplate({
        title: "CFB Pickem 2017",
        publicPath: isDev ? "http://localhost:3000/" : "",
        meta: {
          "name": "CFB pickem",
          "description": "A minimal cfb pickem site"
        }
      })
    }
  }
});

// CSS modules
const cssModulesNames = `${isDev ? "[path][name]__[local]__" : ""}[hash:base64:5]`;

const matchCssLoaders = /(^|!)(css-loader)($|!)/;

const findLoader = (loaders, match) => {
  const found = loaders.filter(l => l && l.loader && l.loader.match(match))
  return found ? found[0] : null;
}
// existing css loader
const cssloader =
  findLoader(config.module.loaders, matchCssLoaders);

const newloader = Object.assign({}, cssloader, {
  test: /\.module\.css$/,
  include: [src],
  loader: cssloader.loader.replace(matchCssLoaders, `$1$2?modules&localIdentName=${cssModulesNames}$3`)
})
config.module.loaders.push(newloader);
cssloader.test = new RegExp(`[^module]${cssloader.test.source}`)
cssloader.loader = newloader.loader

config.module.loaders.push({
  test: /\.css$/,
  include: [modules, css],
  loader: "style!css"
})
// CSS modules

// typescript
config.module.loaders.push({
  test: /\.css$/,
  exclude: /node_modules/,
  loader: "typed-css-modules"
});

config.module.loaders.push({
  test: /\.tsx?$/,
  loaders: [`babel?extends=${path.join(__dirname, '.babelrc-dev')}`, 'ts']
});

config.resolve.extensions = config.resolve.extensions.concat([".ts", ".tsx"]);

// END typescript

config.module.loaders.push({
  test: /\.jpg$/,
  loader: "url-loader?prefix=img/&limit=5000"
});

config.resolve.extensions = config.resolve.extensions.concat([".png", ".jpg"]);

// postcss
config.postcss = [].concat([
  require("precss")({}),
  require("autoprefixer")({}),
  require("cssnano")({})
])
// END postcss

// Roots
config.resolve.root = [src, modules]
config.resolve.alias = {
  "css": join(src, "styles"),
  "containers": join(src, "containers"),
  "components": join(src, "components"),
  "utils": join(src, "utils"),
  "static": join(src, "static"),

  "styles": join(src, "styles")
}
// end Roots

// console.log(require("prettyjson").render(config));

// Testing
if (isTest) {
  config.externals = {
    "react/addons": true,
    "react/lib/ReactContext": true,
    "react/lib/ExecutionEnvironment": true,
  }
  config.module.noParse = /\/sinon\.js/;
  config.resolve.alias["sinon"] = "sinon/pkg/sinon";
  // 125 249 255
  // 0 35 100
  config.plugins = config.plugins.filter(p => {
    const name = p.constructor.toString();
    const fnName = name.match(/^function (.*)\((.*\))/)

    const idx = [
      "DedupePlugin",
      "UglifyJsPlugin"
    ].indexOf(fnName[1]);
    return idx < 0;
  })
}
// End Testing

module.exports = config;