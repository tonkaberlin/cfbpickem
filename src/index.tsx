import { Container } from "./Container";
import * as reactCookie from "./react-cookie/react-cookie";
import { TEAMS } from "./data/teams";
import * as firebase from "firebase";
import * as React from "react";
import { Request, Response } from "express";
import * as express from "express";
const bodyParser = require('body-parser')

const config = {
  apiKey: "AIzaSyAxRmEp5m8rcB9DGco7gRCBgZcfxhwxWv8",
  authDomain: "cfbpickem-264da.firebaseapp.com",
  databaseURL: "https://cfbpickem-264da.firebaseio.com",
  storageBucket: "",
};
const application = firebase.initializeApp(config);

const app = express();
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

app.get("/", (req: Request, res: any) => {

  res.send(`
    <!doctype html>
    <html>
      <head>
        <title>CFB Pickem</title>
      </head>
      <body>
        <div id="root" />
        <script src="/cfbpickem.1.0.0.js"></script>
      </body>
    </html>`
  );

});

app.post("/signin", (req: Request, response: Response) => {

  try {
    application.auth().signInWithEmailAndPassword(req.body.email, req.body.password)
      .then(res => {
        res.getToken().then((token: any) => {
          response.json({ result: token, userId: application.auth().currentUser.uid });
        })
      }).catch(err => {
        console.log(err);
        response.status(400);
        response.json({ result: "success!" });
      });
  } catch (err) {
    console.log(err);
    response.status(500);
    response.json({ result: "failure!" });
  }

});

app.post("/signup", (req: Request, response: Response) => {

  try {
    application.auth().createUserWithEmailAndPassword(req.body.email, req.body.password)
      .then(res => {
        res.getToken().then((token: any) => {
          response.json({ result: token, userId: application.auth().currentUser.uid });
        })
      }).catch(err => {
        console.log(err);
        response.status(400);
        response.json({ result: "success!" });
      });
  } catch (err) {
    console.log(err);
    response.status(500);
    response.json({ result: "failure!" });
  }

});

app.get("/picks", (req: Request, response: Response) => {

  try {
    application.database().ref("picks")
      .child(req.query["week"])
      .child(req.query["uid"])
      .once("value").then(res => {
        response.json(res.val());
      }).catch(err => {
        console.log(err);
        response.status(400);
        response.json({ result: "success!" });
      });
  } catch (err) {
    console.log(err);
    response.status(500);
    response.json({ result: "failure!" });
  }

});

app.get("/games", (req: Request, response: Response) => {

  try {
    application.database().ref("games")
      .child(req.query["week"])
      .once("value").then(res => {
        response.json(res.val());
      }).catch(err => {
        console.log(err);
        response.status(400);
        response.json({ result: "success!" });
      });
  } catch (err) {
    console.log(err);
    response.status(500);
    response.json({ result: "failure!" });
  }

});

app.get("/rankings", (req: Request, response: Response) => {

  try {
    application.database().ref("rankings")
      .child(req.query["week"])
      .once("value").then(res => {
        response.json(res.val());
      }).catch(err => {
        console.log(err);
        response.status(400);
        response.json({ result: "success!" });
      });
  } catch (err) {
    console.log(err);
    response.status(500);
    response.json({ result: "failure!" });
  }

});

app.post("/picks", (req: Request, response: Response) => {

  if (notPastTime()) {
    response.status(403);
    response.json({ result: "forbidden!" });
  } else {
    try {
      const picks = application.database().ref("picks")
        .child(req.body.week)
        .child(req.body.uid);
      picks.set(req.body.picks).then(() => {
        response.json({ result: "success" });
      }).catch(err => {
        console.log(err);
        response.status(400);
        response.json({ result: "success!" });
      });
    } catch (err) {
      console.log(err);
      response.status(500);
      response.json({ result: "failure!" });
    }
  }
});

app.get("/scores", async (req: Request, response: Response) => {

  try {
    let result: any[] = [];
    const users = (await application.database().ref("users").once("value")).val();

    for (let key in users) {
      if (users.hasOwnProperty(key)) {
        const entry: any = { scores: [] };
        entry.name = users[key].name;
        for (let week = 1; week < 14; week++) {
          await Promise.all([
            application.database().ref("picks").child(week + "").once("value"),
            application.database().ref("games").child(week + "").once("value"),
            application.database().ref("rankings").child(week + "").once("value")
          ])
            .then(thing => {
              const picks = thing[0].val();
              const games = thing[1].val();
              const rankings = thing[2].val();

              let score = 0;
              let index = 0;
              for (let gameKey in games) {
                if (games.hasOwnProperty(gameKey) && picks[key]) {
                  const game = games[gameKey];
                  // console.log(`For ${game.team1} vs ${game.team2}, ${entry.name} picked ${picks[key][index].teamId}. The winner was ${game.winner}.`);
                  if (picks[key][index] && game.winner === picks[key][index].teamId) {
                    score += computePoints(game, rankings);
                  }
                  index++;
                }
              };
              entry.scores.push(score);
            });
        }
        result.push(entry);
      }
    }
    response.json(result);

  } catch (err) {
    console.log(err);
    response.status(500);
    response.json({ result: "failure!" });
  }

});

function computePoints(game: any, rankings: any) {
  let total = 0;
  if (TEAMS[game.team1].conference === "ACC") {
    total++;
  }
  if (TEAMS[game.team2].conference === "ACC") {
    total++;
  }
  if (rankings[game.team1]) {
    total++;
  }
  if (rankings[game.team2]) {
    total++;
  }
  if (rankings[game.team1] < 10 && rankings[game.team2] < 10) {
    total++;
  }
  if (rankings[game.team1] < 5 && rankings[game.team2] < 5) {
    total++;
    total++;
  }
  return total;
}

function notPastTime() {
  const currentTime = new Date();
  const gameTime = new Date("2016-12-02T19:00:00-04:00");
  return gameTime.getTime() - currentTime.getTime() < 0;
}

app.listen(3000, () => {
  console.log("Example app listening on port 3000!");
});

const results: string[] = [
"NC State",
"Memphis",
"Washington",
"Air Force",
"Iowa",
"Western Michigan",
"Georgia Tech",
"Kentucky",
"Ohio State",
"Virginia Tech",
"Pittsburgh",
"Boston College",
"Miami (FL)",
"Alabama",
"USC",
"Navy",
"West Virginia",
"Penn State",
"Wisconsin",
"Colorado",
"Clemson",
"Vanderbilt",
"Stanford",
"Florida State",
];

function grade() {
  for (var i = 0; i < results.length; i++) {
    application
      .database()
      .ref("games")
      .child("13")
      .child("" + i)
      .update({ winner: results[i] });
  }
}

grade();

application.database()
  .ref("rankings")
  .child("14")
  .set(
  {
"Alabama": 1,
"Ohio State": 2,
"Clemson": 3,
"Washington": 4,
"Michigan": 5,
"Wisconsin": 6,
"Penn State": 7,
"Colorado": 8,
"Oklahoma": 9,
"Oklahoma State": 10,
"USC": 11,
"Florida State": 12,
"Louisville": 13,
"Auburn": 14,
"Florida": 15,
"West Virginia": 16,
"Western Michigan": 17,
"Stanford": 18,
"Navy": 19,
"Utah": 20,
"LSU": 21,
"Tennessee": 22,
"Virginia Tech": 23,
"Houston": 24,
"Pittsburgh": 25,
  }
  );

application.database()
  .ref("games")
  .child("14")
  .set([
{ "team1": "Western Michigan", "team2": "Ohio", "isConferenceChampionship": true },
{ "team1": "Colorado", "team2": "Washington", "isConferenceChampionship": true },
{ "team1": "Temple", "team2": "Navy", "isConferenceChampionship": true },
{ "team1": "Louisiana Tech", "team2": "Western Kentucky", "isConferenceChampionship": true },
{ "team1": "Oklahoma State", "team2": "Oklahoma" },
{ "team1": "Baylor", "team2": "West Virginia" },
{ "team1": "Alabama", "team2": "Florida", "isConferenceChampionship": true },
{ "team1": "San Diego State", "team2": "Wyoming", "isConferenceChampionship": true },
{ "team1": "Clemson", "team2": "Virginia Tech", "isConferenceChampionship": true },
{ "team1": "Wisconsin", "team2": "Penn State", "isConferenceChampionship": true },
  ]);