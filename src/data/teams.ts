import { app } from "firebase";
import FirebaseApplication = app.App;

export const TEAMS: Teams = {
  "Abilene Christian University": {
    "conference": "Southland"
  },
  "Air Force": {
    "conference": "Mountain West"
  },
  "Akron": {
    "conference": "MAC"
  },
  "Alabama A&M": {
    "conference": "SWAC"
  },
  "Alabama State University": {
    "conference": "SWAC"
  },
  "Alabama": {
    "conference": "SEC"
  },
  "Alcorn State": {
    "conference": "SWAC"
  },
  "Appalachian State": {
    "conference": "Sun Belt"
  },
  "Arizona State": {
    "conference": "Pac-12"
  },
  "Arizona": {
    "conference": "Pac-12"
  },
  "Arkansas State": {
    "conference": "Sun Belt"
  },
  "Arkansas": {
    "conference": "SEC"
  },
  "Army": {
    "conference": "Independent"
  },
  "Auburn": {
    "conference": "SEC"
  },
  "Austin Peay State University": {
    "conference": "Ohio Valley"
  },
  "Ball State": {
    "conference": "MAC"
  },
  "Baylor": {
    "conference": "Big 12"
  },
  "Bethune-Cookman University": {
    "conference": "MEAC"
  },
  "Boise State": {
    "conference": "Mountain West"
  },
  "Boston College": {
    "conference": "ACC"
  },
  "Bowling Green": {
    "conference": "MAC"
  },
  "Brown University": {
    "conference": "Ivy"
  },
  "Bryant University": {
    "conference": "Northeast"
  },
  "Bucknell University": {
    "conference": "Patriot"
  },
  "Buffalo": {
    "conference": "MAC"
  },
  "Butler University": {
    "conference": "Pioneer"
  },
  "BYU": {
    "conference": "Independent"
  },
  "California Polytechnic State University": {
    "conference": "Big Sky"
  },
  "California State University": {
    "conference": "Big Sky"
  },
  "California": {
    "conference": "Pac-12"
  },
  "Campbell University": {
    "conference": "Pioneer"
  },
  "Central Connecticut State University": {
    "conference": "Northeast"
  },
  "Central Michigan": {
    "conference": "MAC"
  },
  "Charleston Southern": {
    "conference": "Big South"
  },
  "Charlotte": {
    "conference": "C-USA"
  },
  "Cincinnati": {
    "conference": "American"
  },
  "Clemson": {
    "conference": "ACC"
  },
  "Coastal Carolina University": {
    "conference": "FCS Independent"
  },
  "Colgate": {
    "conference": "Patriot"
  },
  "College of the Holy Cross": {
    "conference": "Patriot"
  },
  "William & Mary": {
    "conference": "CAA"
  },
  "Colorado State": {
    "conference": "Mountain West"
  },
  "Colorado": {
    "conference": "Pac-12"
  },
  "Columbia University": {
    "conference": "Ivy"
  },
  "Connecticut": {
    "conference": "American"
  },
  "Cornell University": {
    "conference": "Ivy"
  },
  "Dartmouth College": {
    "conference": "Ivy"
  },
  "Davidson College": {
    "conference": "Pioneer"
  },
  "Delaware State University": {
    "conference": "MEAC"
  },
  "Drake University": {
    "conference": "Pioneer"
  },
  "Duke": {
    "conference": "ACC"
  },
  "Duquesne University": {
    "conference": "Northeast"
  },
  "East Carolina": {
    "conference": "American"
  },
  "East Tennessee State University": {
    "conference": "Southern"
  },
  "Eastern Illinois University": {
    "conference": "Ohio Valley"
  },
  "Eastern Kentucky University": {
    "conference": "Ohio Valley"
  },
  "Eastern Michigan": {
    "conference": "MAC"
  },
  "Eastern Washington University": {
    "conference": "Big Sky"
  },
  "Elon University": {
    "conference": "CAA"
  },
  "FIU": {
    "conference": "C-USA"
  },
  "Florida A&M": {
    "conference": "MEAC"
  },
  "Florida Atlantic": {
    "conference": "C-USA"
  },
  "Florida State": {
    "conference": "ACC"
  },
  "Florida": {
    "conference": "SEC"
  },
  "Fordham University": {
    "conference": "Patriot"
  },
  "Fresno State": {
    "conference": "Mountain West"
  },
  "Furman": {
    "conference": "Southern"
  },
  "Gardner–Webb University": {
    "conference": "Big South"
  },
  "Georgetown University": {
    "conference": "Patriot"
  },
  "Georgia Southern": {
    "conference": "Sun Belt"
  },
  "Georgia State": {
    "conference": "Sun Belt"
  },
  "Georgia Tech": {
    "conference": "ACC"
  },
  "Georgia": {
    "conference": "SEC"
  },
  "Grambling State University": {
    "conference": "SWAC"
  },
  "Hampton University": {
    "conference": "MEAC"
  },
  "Harvard University": {
    "conference": "Ivy"
  },
  "Hawaii": {
    "conference": "Mountain West"
  },
  "Houston Baptist University": {
    "conference": "Southland"
  },
  "Houston": {
    "conference": "American"
  },
  "Howard University": {
    "conference": "MEAC"
  },
  "Idaho State University": {
    "conference": "Big Sky"
  },
  "Idaho": {
    "conference": "Sun Belt"
  },
  "Illinois State University": {
    "conference": "Missouri Valley"
  },
  "Illinois": {
    "conference": "Big Ten"
  },
  "Indiana State University": {
    "conference": "Missouri Valley"
  },
  "Indiana": {
    "conference": "Big Ten"
  },
  "Iowa State": {
    "conference": "Big 12"
  },
  "Iowa": {
    "conference": "Big Ten"
  },
  "Jackson State University": {
    "conference": "SWAC"
  },
  "Jacksonville State": {
    "conference": "Ohio Valley"
  },
  "Jacksonville University": {
    "conference": "Pioneer"
  },
  "JMU": {
    "conference": "CAA"
  },
  "Kansas State": {
    "conference": "Big 12"
  },
  "Kansas": {
    "conference": "Big 12"
  },
  "Kennesaw State University": {
    "conference": "Big South"
  },
  "Kent State": {
    "conference": "MAC"
  },
  "Kentucky": {
    "conference": "SEC"
  },
  "Lafayette College": {
    "conference": "Patriot"
  },
  "Lamar": {
    "conference": "Southland"
  },
  "Lehigh University": {
    "conference": "Patriot"
  },
  "Liberty": {
    "conference": "Big South"
  },
  "Louisiana Tech": {
    "conference": "C-USA"
  },
  "Louisiana-Lafayette": {
    "conference": "Sun Belt"
  },
  "Louisiana-Monroe": {
    "conference": "Sun Belt"
  },
  "Louisville": {
    "conference": "ACC"
  },
  "LSU": {
    "conference": "SEC"
  },
  "Marist College": {
    "conference": "Pioneer"
  },
  "Marshall": {
    "conference": "C-USA"
  },
  "Maryland": {
    "conference": "Big Ten"
  },
  "Massachusetts": {
    "conference": "Independent"
  },
  "McNeese State University": {
    "conference": "Southland"
  },
  "Memphis": {
    "conference": "American"
  },
  "Mercer": {
    "conference": "Southern"
  },
  "Miami (FL)": {
    "conference": "ACC"
  },
  "Miami (OH)": {
    "conference": "MAC"
  },
  "Michigan State": {
    "conference": "Big Ten"
  },
  "Michigan": {
    "conference": "Big Ten"
  },
  "Middle Tennessee": {
    "conference": "C-USA"
  },
  "Minnesota": {
    "conference": "Big Ten"
  },
  "Mississippi State": {
    "conference": "SEC"
  },
  "Mississippi Valley State University": {
    "conference": "SWAC"
  },
  "Missouri State University": {
    "conference": "Missouri Valley"
  },
  "Missouri": {
    "conference": "SEC"
  },
  "Monmouth University": {
    "conference": "Big South"
  },
  "Montana State University": {
    "conference": "Big Sky"
  },
  "Morehead State University": {
    "conference": "Pioneer"
  },
  "Morgan State University": {
    "conference": "MEAC"
  },
  "Murray State University": {
    "conference": "Ohio Valley"
  },
  "Navy": {
    "conference": "American"
  },
  "NC State": {
    "conference": "ACC"
  },
  "Nebraska": {
    "conference": "Big Ten"
  },
  "Nevada": {
    "conference": "Mountain West"
  },
  "New Mexico State": {
    "conference": "Sun Belt"
  },
  "New Mexico": {
    "conference": "Mountain West"
  },
  "Nicholls": {
    "conference": "Southland"
  },
  "NIU": {
    "conference": "MAC"
  },
  "Norfolk State University": {
    "conference": "MEAC"
  },
  "North Carolina Agricultural and Technical State University": {
    "conference": "MEAC"
  },
  "North Carolina Central": {
    "conference": "MEAC"
  },
  "North Carolina": {
    "conference": "ACC"
  },
  "North Dakota State": {
    "conference": "Missouri Valley"
  },
  "North Texas": {
    "conference": "C-USA"
  },
  "Northern Arizona University": {
    "conference": "Big Sky"
  },
  "Northwestern State": {
    "conference": "Southland"
  },
  "Northwestern": {
    "conference": "Big Ten"
  },
  "Notre Dame": {
    "conference": "Independent"
  },
  "Ohio State": {
    "conference": "Big Ten"
  },
  "Ohio": {
    "conference": "MAC"
  },
  "Oklahoma State": {
    "conference": "Big 12"
  },
  "Oklahoma": {
    "conference": "Big 12"
  },
  "Old Dominion": {
    "conference": "C-USA"
  },
  "Ole Miss": {
    "conference": "SEC"
  },
  "Oregon State": {
    "conference": "Pac-12"
  },
  "Oregon": {
    "conference": "Pac-12"
  },
  "Penn State": {
    "conference": "Big Ten"
  },
  "Pittsburgh": {
    "conference": "ACC"
  },
  "Portland State": {
    "conference": "Big Sky"
  },
  "Prairie View A&M": {
    "conference": "SWAC"
  },
  "Presbyterian College": {
    "conference": "Big South"
  },
  "Princeton University": {
    "conference": "Ivy"
  },
  "Purdue": {
    "conference": "Big Ten"
  },
  "Rice": {
    "conference": "C-USA"
  },
  "Robert Morris University": {
    "conference": "Northeast"
  },
  "Rutgers": {
    "conference": "Big Ten"
  },
  "Sacred Heart University": {
    "conference": "Northeast"
  },
  "Saint Francis University": {
    "conference": "Northeast"
  },
  "Sam Houston State University": {
    "conference": "Southland"
  },
  "Samford University": {
    "conference": "Southern"
  },
  "San Diego State": {
    "conference": "Mountain West"
  },
  "San Jose State": {
    "conference": "Mountain West"
  },
  "Savannah State University": {
    "conference": "MEAC"
  },
  "SMU": {
    "conference": "American"
  },
  "South Alabama": {
    "conference": "Sun Belt"
  },
  "South Carolina State": {
    "conference": "MEAC"
  },
  "South Carolina": {
    "conference": "SEC"
  },
  "South Dakota State": {
    "conference": "Missouri Valley"
  },
  "South Florida": {
    "conference": "American"
  },
  "Southeast Missouri State University": {
    "conference": "Ohio Valley"
  },
  "Southeastern Louisiana": {
    "conference": "Southland"
  },
  "Southern Illinois University Carbondale": {
    "conference": "Missouri Valley"
  },
  "Southern Miss": {
    "conference": "C-USA"
  },
  "Southern University": {
    "conference": "SWAC"
  },
  "Southern Utah University": {
    "conference": "Big Sky"
  },
  "Stanford": {
    "conference": "Pac-12"
  },
  "Stephen F Austin State University": {
    "conference": "Southland"
  },
  "Stetson University": {
    "conference": "Pioneer"
  },
  "Stony Brook University": {
    "conference": "CAA"
  },
  "Syracuse": {
    "conference": "ACC"
  },
  "TCU": {
    "conference": "Big 12"
  },
  "Temple": {
    "conference": "American"
  },
  "Tennessee State University": {
    "conference": "Ohio Valley"
  },
  "Tennessee Technological University": {
    "conference": "Ohio Valley"
  },
  "Tennessee": {
    "conference": "SEC"
  },
  "Texas A&M": {
    "conference": "SEC"
  },
  "Texas Southern University": {
    "conference": "SWAC"
  },
  "Texas State": {
    "conference": "Sun Belt"
  },
  "Texas Tech": {
    "conference": "Big 12"
  },
  "Texas": {
    "conference": "Big 12"
  },
  "The Citadel": {
    "conference": "Southern"
  },
  "Toledo": {
    "conference": "MAC"
  },
  "Towson University": {
    "conference": "CAA"
  },
  "Troy": {
    "conference": "Sun Belt"
  },
  "Tulane": {
    "conference": "American"
  },
  "Tulsa": {
    "conference": "American"
  },
  "UCF": {
    "conference": "American"
  },
  "UCLA": {
    "conference": "Pac-12"
  },
  "University at Albany": {
    "conference": "CAA"
  },
  "University of Arkansas at Pine Bluff": {
    "conference": "SWAC"
  },
  "UC Davis": {
    "conference": "Big Sky"
  },
  "University of Central Arkansas": {
    "conference": "Southland"
  },
  "University of Dayton": {
    "conference": "Pioneer"
  },
  "Delaware": {
    "conference": "CAA"
  },
  "University of Maine": {
    "conference": "CAA"
  },
  "University of Montana": {
    "conference": "Big Sky"
  },
  "University of New Hampshire": {
    "conference": "CAA"
  },
  "University of North Dakota": {
    "conference": "Big Sky"
  },
  "University of Northern Colorado": {
    "conference": "Big Sky"
  },
  "University of Northern Iowa": {
    "conference": "Missouri Valley"
  },
  "University of Pennsylvania": {
    "conference": "Ivy"
  },
  "University of Rhode Island": {
    "conference": "CAA"
  },
  "Richmond": {
    "conference": "CAA"
  },
  "University of San Diego": {
    "conference": "Pioneer"
  },
  "University of South Dakota": {
    "conference": "Missouri Valley"
  },
  "Chattanooga": {
    "conference": "Southern"
  },
  "University of Tennessee at Martin": {
    "conference": "Ohio Valley"
  },
  "University of the Incarnate Word": {
    "conference": "Southland"
  },
  "UNLV": {
    "conference": "Mountain West"
  },
  "USC": {
    "conference": "Pac-12"
  },
  "Utah State": {
    "conference": "Mountain West"
  },
  "Utah": {
    "conference": "Pac-12"
  },
  "UTEP": {
    "conference": "C-USA"
  },
  "UTSA": {
    "conference": "C-USA"
  },
  "Valparaiso University": {
    "conference": "Pioneer"
  },
  "Vanderbilt": {
    "conference": "SEC"
  },
  "Villanova": {
    "conference": "CAA"
  },
  "Virginia Military Institute": {
    "conference": "Southern"
  },
  "Virginia Tech": {
    "conference": "ACC"
  },
  "UVA": {
    "conference": "ACC"
  },
  "Wagner": {
    "conference": "Northeast"
  },
  "Wake Forest": {
    "conference": "ACC"
  },
  "Washington State": {
    "conference": "Pac-12"
  },
  "Washington": {
    "conference": "Pac-12"
  },
  "Weber State University": {
    "conference": "Big Sky"
  },
  "West Virginia": {
    "conference": "Big 12"
  },
  "Western Carolina University": {
    "conference": "Southern"
  },
  "Western Illinois University": {
    "conference": "Missouri Valley"
  },
  "Western Kentucky": {
    "conference": "C-USA"
  },
  "Western Michigan": {
    "conference": "MAC"
  },
  "Wisconsin": {
    "conference": "Big Ten"
  },
  "Wofford": {
    "conference": "Southern"
  },
  "Wyoming": {
    "conference": "Mountain West"
  },
  "Yale University": {
    "conference": "Ivy"
  },
  "Youngstown State University": {
    "conference": "Missouri Valley"
  }
};

export function putPicks(application: FirebaseApplication) {
  const teams = application.database().ref("teams");
  teams.set(TEAMS);
}

interface Teams {
  [key: string]: Team,
}

interface Team {
  conference: string;
}