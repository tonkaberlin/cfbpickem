import * as React from "react";
import * as cookie from "./react-cookie/react-cookie";
import {Landing} from "./react/Landing";
import {Scoreboard} from "./react/Scoreboard";
import {TEAMS} from "./data/teams";

const WEEK = 14;
const OVERRIDE_UID = "";

export class Container extends React.Component<{}, ContainerState> {

    constructor() {
        super();

        this.state = {
            token: cookie.load("token"),
            userId: OVERRIDE_UID ? OVERRIDE_UID : cookie.load("uid"),
            picks: [],
            games: [],
            rankings: {},
        };

        if (this.state.token) {
            this.getPicks();
            this.getGames();
            this.getRankings();
        }
    }

    public render() {

        if (this.state.token) {
            if (this.state.picks) {
                return (
                    <div>
                        <h1>Conference Championship Week (Week {WEEK})</h1>
                        <div>
                            {this.state.games.map((g, index) => {
                                return <div><span onClick={this.onClickThing.bind(this, index, g.team1) }>{this.getFormattedName(index, g.team1) }</span> vs <span onClick={this.onClickThing.bind(this, index, g.team2) }>{this.getFormattedName(index, g.team2) }</span> ({this.computePoints(g) } Points) </div>
                            }) }
                        </div>
                        <br />
                        <button onClick={this.sendPicks}>Submit</button>
                        <br /><br />
                        <Scoreboard />
                        <br /><br />
                        <button onClick={this.signOut}>Sign Out</button>
                    </div>
                );
            } else {
                return (<div>Loading..</div>);
            }

        } else {
            return (
                <Landing setUserId={this.setUserId}/>
            );
        }
    }

    public getFormattedName = (index: number, t: string) => {
        if (this.state.picks[index] && this.state.picks[index].teamId === t) {
            return (<span><b>{this.getRank(t, 1) }</b></span>);
        } else {
            return (<span>{this.getRank(t, 1) }</span>);
        }
    }

    public setUserId = (userId: string, token: string) => {
        this.state.userId = userId;
        cookie.save("uid", userId);
        cookie.save("token", token);
        this.state.token = token;
        this.getPicks();
        this.getGames();
        this.getRankings();
        this.setState(this.state);
    }

    public onClickThing = (gameId: number, teamId: string) => {
        const userId = this.state.userId;
        this.state.picks[gameId] = { gameId, teamId };
        this.setState(this.state);
    }

    private isPicked = (gameId: number, teamId: string) => {
        return this.state.picks[gameId] && this.state.picks[gameId].teamId === teamId;
    }

    public sendPicks = () => {
        var http = new XMLHttpRequest();

        http.open("POST", "/picks", true);
        http.setRequestHeader("Content-type", "application/json");

        http.onreadystatechange = () => {
            if (http.readyState == 4 && http.status == 200) {
                const body = JSON.parse(http.responseText);
            } else if (http.readyState == 4 && http.status == 400) {
                alert("error 400");
            } else if (http.readyState == 4 && http.status == 403) {
                alert("Picks are Locked");
            } else if (http.readyState == 4 && http.status == 500) {
                alert("error 500");
            }
        }
        http.send(JSON.stringify({ week: WEEK, uid: this.state.userId, picks: this.state.picks }));
    }

    public getPicks = () => {
        var http = new XMLHttpRequest();

        http.open("GET", "/picks?week=" + WEEK + "&uid=" + this.state.userId, true);
        http.setRequestHeader("Content-type", "application/json");

        http.onreadystatechange = () => {
            if (http.readyState == 4 && http.status == 200) {
                const body = JSON.parse(http.responseText);
                this.state.picks = body ? body : [];
                this.setState(this.state);
            }
        }
        http.send();
    }

    public getGames = () => {
        var http = new XMLHttpRequest();

        http.open("GET", "/games?week=" + WEEK, true);
        http.setRequestHeader("Content-type", "application/json");

        http.onreadystatechange = () => {
            if (http.readyState == 4 && http.status == 200) {
                const body = JSON.parse(http.responseText);
                this.state.games = body ? body : [];
                this.setState(this.state);
            }
        }
        http.send();
    }

    public getRankings = () => {
        var http = new XMLHttpRequest();

        http.open("GET", "/rankings?week=" + WEEK, true);
        http.setRequestHeader("Content-type", "application/json");

        http.onreadystatechange = () => {
            if (http.readyState == 4 && http.status == 200) {
                const body = JSON.parse(http.responseText);
                this.state.rankings = body ? body : {};
                this.setState(this.state);
            }
        }
        http.send();
    }

    public signOut = () => {
        cookie.remove("token");
        this.state.token = undefined;
        this.state.picks = undefined;
        this.setState(this.state);
    }

    public getRank = (t: string, week: number) => {
        if (this.state.rankings[t]) {
            return "#" + this.state.rankings[t] + " " + t;
        } else {
            return t;
        }
    }

    public computePoints = (g: Game) => {
        let total = 0;
        console.log(g.team1 + " at " + g.team2);
        if (TEAMS[g.team1].conference === "ACC") {
            total++;
        }
        if (TEAMS[g.team2].conference === "ACC") {
            total++;
        }
        if (this.state.rankings[g.team1]) {
            total++;
        }
        if (this.state.rankings[g.team2]) {
            total++;
        }
        if (this.state.rankings[g.team1] < 10 && this.state.rankings[g.team2] < 10) {
            total++;
        }
        if (this.state.rankings[g.team1] < 5 && this.state.rankings[g.team2] < 5) {
            total++;
            total++;
        }
        return total;
    }
}



interface ContainerState {
    token?: string;
    picks?: Pick[];
    userId?: string;
    games?: Game[];
    rankings: Rankings;
}

interface Rankings {
    [key: string]: number,
}

interface Game {
    team1: string;
    team2: string;
}

interface Team {
    conference: string;
}

interface Pick {
    gameId: number;
    teamId: string;
}
