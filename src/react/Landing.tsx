import * as React from "react";
import * as cookie from "../react-cookie/react-cookie";

const divStyle = {
    // color: 'white',
    // backgroundColor: '#7B1FA2',
    fontFamily: '"Lucida Sans Unicode", "Lucida Grande", sans-serif',
    width: '100vw',
    height: '100vh'
};

export class Landing extends React.Component<LandingProps, LandingState> {

    constructor() {
        super();
        this.state = {};
    }

    public render() {
        return (
            <div style={divStyle}>
                <h1>Welcome to CFB Pickem 2016</h1>
                <label>Email<input onChange={this.onChangeEmail} ref="email" /></label><br /><br />
                <label>Password<input type="password" onChange={this.onChangePassword} ref="password" onKeyDown={this.onKeyDown} /></label><br /><br />
                {this.state.error ? <div style={{ color: "red", marginBottom: "20" }}>{this.state.error}</div> : ""}
                <button onClick={this.signIn}>Sign In</button>< br /><br />
            </div>
        );
    }

    public signIn = () => {
        const http = new XMLHttpRequest();

        http.open("POST", "/signin", true);
        http.setRequestHeader("Content-type", "application/json");

        http.onreadystatechange = () => {
            if (http.readyState === 4 && http.status === 200) {
                const body = JSON.parse(http.responseText);
                this.props.setUserId(body.userId, body.result);
                this.setState(this.state);
            } else if (http.readyState === 4) {
                this.state.error = "Invalid Email/Password";
                this.setState(this.state);
            }
        }
        http.send(JSON.stringify(this.state));
    }

    public signUp = () => {
        const http = new XMLHttpRequest();

        http.open("POST", "/signup", true);
        http.setRequestHeader("Content-type", "application/json");

        http.onreadystatechange = () => {
            if (http.readyState === 4 && http.status === 200) {
                const body = JSON.parse(http.responseText);
                this.props.setUserId(body.userId, body.result);
            } else if (http.readyState === 4) {
                this.state.error = "Invalid Email/Password";
                this.setState(this.state);
            }
        }
        http.send(JSON.stringify(this.state));
    }

    public onChangeEmail = (evt: any) => {
        this.state.email = evt.target.value;
    }

    public onChangePassword = (evt: any) => {
        this.state.password = evt.target.value;
    }

    public onKeyDown = (evt: any) => {
        if (evt.keyCode === 13) {
            this.signIn();
        }
    }

}

interface LandingProps {
    setUserId: (userId: string, token: string) => void
}

interface LandingState {
    email?: string;
    password?: string;
    error?: string;
}