import * as React from "react";
import * as cookie from "../react-cookie/react-cookie";

const WEEK = 14;

export class Top25 extends React.Component<Props, State> {

    constructor() {
        super();
        this.state = {};
        this.getScores();
    }

    public render() {
        if (this.state.users) {

            const rows: any[] = [];
            rows.push(<tr>
                {this.state.users.map(user => <td style={{ padding: "10px" }}><b>{user.name}</b></td>) }
            </tr>);
            for (let week = 0; week < WEEK - 1; week++) {
                rows.push(<tr>
                    {this.state.users.map(user => <td style={{ padding: "10px" }}>{user.scores[week]}</td>) }
                </tr>);
            }

            return (
                <div>
                    {rows}
                </div>
            );
        } else {
            return (
                <div>
                    Loading scores..
                </div>
            );
        }
    }

    private getScores = () => {
        const http = new XMLHttpRequest();

        http.open("GET", "/scores", true);
        http.setRequestHeader("Content-type", "application/json");

        http.onreadystatechange = () => {
            if (http.readyState === 4 && http.status === 200) {
                const body = JSON.parse(http.responseText);
                this.setState({ users: body });
            }
        }
        http.send(JSON.stringify(this.state));
    }

}

interface Props {
    setUserId?: (userId: string, token: string) => void
}

interface State {
    email?: string;
    password?: string;
    error?: string;
    users?: User[];
}

interface User {
    name: string;
    scores: Score[];
}

interface Score {
    week: number;
    score: number;
}