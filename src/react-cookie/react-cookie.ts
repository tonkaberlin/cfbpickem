import * as cookie from "cookie";

interface RawCookie {
    [key: string]: string;
}

let _rawCookie: RawCookie = {};
let _res: any = undefined;

function _isResWritable() {
    if (!_res)
        return false
    if (_res.headersSent === true)
        return false
    return true
}

export function load(name: string, doNotParse?: boolean) {
    const cookies = (typeof document === "undefined") ? _rawCookie : cookie.parse(document.cookie);
    let cookieVal = cookies && cookies[name];

    if (!doNotParse) {
        try {
            cookieVal = JSON.parse(cookieVal);
        } catch (e) {
            // Not serialized object
        }
    }

    return cookieVal;
}

export function select(regex: RegExp) {
    const cookies = (typeof document === "undefined") ? _rawCookie : cookie.parse(document.cookie);
    if (!cookies)
        return {}
    if (!regex)
        return cookies
    return Object.keys(cookies)
        .reduce(function (accumulator, name) {
            if (!regex.test(name))
                return accumulator
            const newCookie: RawCookie = {}
            newCookie[name] = cookies[name]
            return Object.assign({}, accumulator, newCookie)
        }, {})
}

export function save(name: string, val: string, opt?: CookieSerializeOptions) {
    _rawCookie[name] = val;

    // allow you to work with cookies as objects.
    if (typeof val === "object") {
        _rawCookie[name] = JSON.stringify(val);
    }

    // Cookies only work in the browser
    if (typeof document !== "undefined") {
        document.cookie = cookie.serialize(name, _rawCookie[name], opt);
    }

    if (_isResWritable() && _res.cookie) {
        _res.cookie(name, val, opt);
    }
}

export function remove(name: string, opt?: any) {
    delete _rawCookie[name];

    if (typeof opt === "undefined") {
        opt = {};
    } else if (typeof opt === "string") {
        // Will be deprecated in future versions
        opt = { path: opt };
    } else {
        // Prevent mutation of opt below
        opt = Object.assign({}, opt);
    }

    if (typeof document !== "undefined") {
        opt.expires = new Date(1970, 1, 1, 0, 0, 1);
        document.cookie = cookie.serialize(name, "", opt);
    }

    if (_isResWritable() && _res.clearCookie) {
        _res.clearCookie(name, opt);
    }
}

export function setRawCookie(rawCookie: string) {
    if (rawCookie) {
        _rawCookie = cookie.parse(rawCookie);
    } else {
        _rawCookie = {};
    }
}

export function plugToRequest(req: any, res: any) {
    if (req.cookie) {
        _rawCookie = req.cookie;
    } else if (req.cookies) {
        _rawCookie = req.cookies;
    } else if (req.headers && req.headers.cookie) {
        setRawCookie(req.headers.cookie);
    } else {
        _rawCookie = {};
    }

    _res = res;
    return () => {
        _res = null;
        _rawCookie = {};
    }
}

if (typeof window !== "undefined") {
    // window["reactCookie"] = reactCookie;
}